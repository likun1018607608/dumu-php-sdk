<?php


namespace timkeji;




class Dumu
{

    private $config = [
        'ip'=>'',//IP
        'pass'=>'',//pass
    ];

    protected $host = '';

    protected $api = [
        'deviceManage'=>[
            'getDeviceID'=>"deviceManage/getDeviceID", //获取序列号
            'setPassword'=>'deviceManage/setPassword', //设置密码
            'setTime'=>'deviceManage/setTime', //设置时间
            'restart'=>'deviceManage/restart', //设备重启
            'setLogo'=>'deviceManage/setLogo', //公司LOGO设置
            'setDisplayImg'=>'deviceManage/setDisplayImg', //设置屏保图片,
            'setBootupRegMethod'=>'deviceManage/setBootupRegMethod', //设置默认识别模式
            'setPrompting'=>'deviceManage/setPrompting', //设置自定义提示信息
            'setRecognitionCallback'=>'deviceManage/setRecognitionCallback', //设置识别记录回调
            'setDeviceHeartBeat'=>'deviceManage/setDeviceHeartBeat',//设置设备心跳回调地址
            'setUserCallback'=>'deviceManage/setUserCallback', //设置用户信息变更回调
            'getSoftVersion'=>'deviceManage/getSoftVersion', //设置软件版本
        ],
        'userManage'=>[
            'addUser'=>'userManage/addUser',//用户注册或更新
            'deleteUser'=>'userManage/deleteUser',//单用户删除
            'getUserInfo'=>'userManage/getUserInfo', //获取用户信息
            'setUserAuthTime'=>'userManage/setUserAuthTime', //设置用户有效期
            'getUserList'=>'userManage/getUserList', //获取用户列表
            'deleteUserImages'=>'userManage/deleteUserImages', //删除用户数据
            'deleteUserInfo'=>'userManage/deleteUserInfo',//批量删除用户信息
            'deleteAllUserInfo'=>'userManage/deleteAllUserInfo',//清空全部用户信息
        ],
        'recognitionManage'=>[
            'getFeature'=>'recognitionManage/getFeature',//获取人脸特征值
            'match'=>'recognitionManage/match',//人脸对比
            'identify'=>'recognitionManage/identify',//人脸搜索
            'setDetectNum'=>'recognitionManage/setDetectNum',//多人脸检测
            'setLivenessType'=>'recognitionManage/setLivenessType',//设置活体检测模式
            'setQualityControl'=>'recognitionManage/setQualityControl',//设置质量控制
            'setStranger'=>'recognitionManage/setStranger',//陌生人开关
            'setMinSize'=>'recognitionManage/setMinSize',//设置最小人脸
            'setIntervalTime'=>'recognitionManage/setIntervalTime',//设置识别间隔时间
            'setRecognitionParam'=>'recognitionManage/setRecognitionParam',//设置识别阈值
            'setOutControl'=>'recognitionManage/setOutControl',//设置IO远程控制
            'setIOswitch'=>'recognitionManage/setIOswitch',//设置IO开关
        ],
        'recordManage'=>[
            'searchRecords'=>'recordManage/searchRecords',//识别记录
            'deleteRecords'=>'recordManage/deleteRecords',//删除记录
            'deleteAllRecords'=>'recordManage/deleteAllRecords',//识别记录清空
        ]
    ];

    //初始化示例
    public function __construct($config = [])
    {
        if (is_array($config)){
            $this->config = array_merge( $this->config,$config);
        }
        $this->host= 'http://'.$this->config['ip'] .':8080/';

    }


    //获取设备信息
    public function info(){

        $url = $this->host.$this->api['deviceManage']['getDeviceID'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass']]);

        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }

    }
    //获取设备软件版本
    public function getSoftVersion(){
        $url = $this->host.$this->api['deviceManage']['getSoftVersion'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass']]);

        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }
    //设置设备密码
    public function setPassword($old,$new){
        $url = $this->host.$this->api['deviceManage']['setPassword'];

        $i = $this->curl_post($url,['old_password'=>$old,'new_password'=>$new]);

        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }
    //设备时间
    public function setTime(){
        $url = $this->host.$this->api['deviceManage']['setTime'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'timestamp'=>time()]);

        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }
    public function restart(){
        $url = $this->host.$this->api['deviceManage']['restart'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass']]);

        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //设置LOGO
    /*
     * start  base64编码 (无头部) jpg格式
     * stop   base64编码 (无头部) jpg格式
     * text   不超过32字节
     *
     * */
    public function setLogo($start,$top,$text){
        $url = $this->host.$this->api['deviceManage']['setLogo'];
        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'image_start'=>$start,
            'image_top'=>$top,'top_text'=>$text]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //设置屏保
    public function setDisplayImg($cfg){
        $url = $this->host.$this->api['deviceManage']['setDisplayImg'];
        $arr = array_merge(['pass'=>$this->config['pass']],$cfg);
        $i = $this->curl_post($url,$arr);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //设置默认识别模式
    public function setBootupRegMethod($method){
        $url = $this->host.$this->api['deviceManage']['setBootupRegMethod'];
        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'method'=>$method]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }


    /*
     * 设置自定义提示信息
     * */
    public function setPrompting($cfg){
        $url = $this->host.$this->api['deviceManage']['setPrompting'];
        $arr = array_merge(['pass'=>$this->config['pass']],$cfg);
        $i = $this->curl_post($url,$arr);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //设置识别记录回调
    public function setRecognitionCallback($callback,$interval_time){
        $url = $this->host.$this->api['deviceManage']['setRecognitionCallback'];
        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'callback_url'=>$callback,'interval_time'=>$interval_time]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //设置设备心跳回调地址
    public function setDeviceHeartBeat($callback,$interval_time){
        $url = $this->host.$this->api['deviceManage']['setDeviceHeartBeat'];
        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'callback_url'=>$callback,'interval_time'=>$interval_time]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }
    //设置用户信息变更回调
    public function setUserCallback($callback){
        $url = $this->host.$this->api['deviceManage']['setDeviceHeartBeat'];
        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'callback_url'=>$callback]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }


    //设备管理结束

    //用户管理开始
    public function addUser($cfg){
        $url = $this->host.$this->api['userManage']['addUser'];
        $arr = array_merge(['pass'=>$this->config['pass']],$cfg);
        $i = $this->curl_post($url,$arr);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }
    //单用户删除
    public function deleteUser($uid){
        $url = $this->host.$this->api['userManage']['deleteUser'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'user_id'=>$uid]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //获取用户信息
    public function getUserInfo($uid){
        $url = $this->host.$this->api['userManage']['getUserInfo'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'user_id'=>$uid]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //设置用户有效期

    public function setUserAuthTime($uid,$start_time,$end_time){
        $url = $this->host.$this->api['userManage']['setUserAuthTime'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'user_id'=>$uid,'auth_start_time'=>$start_time,'auth_end_time'=>$end_time]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //获取用户列表
    public function getUserList($start,$length){
        $url = $this->host.$this->api['userManage']['getUserList'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'start'=>$start,'length'=>$length]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //删除用户图片特征数据
    public function deleteUserImages($uid){
        $url = $this->host.$this->api['userManage']['deleteUserImages'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'user_id'=>$uid]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //批量删除用户信息
    public function deleteUserInfo($userlist){
        $url = $this->host.$this->api['userManage']['deleteUserInfo'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass'],'user_id'=>[$userlist]]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //清空全部用户信息
    public function deleteAllUserInfo(){
        $url = $this->host.$this->api['userManage']['deleteAllUserInfo'];

        $i = $this->curl_post($url,['pass'=>$this->config['pass']]);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //用户管理相关结束

    //识别记录查询
    public function searchRecords($cfg){
        $url = $this->host.$this->api['recordManage']['searchRecords'];
        $arr = array_merge(['pass'=>$this->config['pass']],$cfg);
        $i = $this->curl_post($url,$arr);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }

    //删除用户记录 可设置时段

    public function deleteRecords($cfg){
        $url = $this->host.$this->api['recordManage']['deleteRecords'];
        $arr = array_merge(['pass'=>$this->config['pass']],$cfg);
        $i = $this->curl_post($url,$arr);
        $req = json_decode($i);
        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }


    //删除指定用户或全部用户的识别记录
    public function deleteAllRecords($uid=null){
        $url = $this->host.$this->api['recordManage']['deleteAllRecords'];

        if ($uid){

            $i = $this->curl_post($url,['pass'=>$this->config['pass'],'user_id'=>$uid]);
        }else{

            $i = $this->curl_post($url,['pass'=>$this->config['pass']]);
        }

        $req = json_decode($i);

        if ($req->code){
            return  $req;
        }else{
            return false;
        }
    }



    public function curl_post($url, $data)
    {

        $ch = curl_init();
        $header = array(
            'Content-Type: application/json; charset=utf-8',
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        // POST数据

        curl_setopt($ch, CURLOPT_POST, 1);

        // 把post的变量加上
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $output = curl_exec($ch);
        if($output == false) {
            curl_close($ch);
            return json_encode(['code'=>false,'msg'=>'http error']);

        }
        curl_close($ch);

        return $output;
    }
}